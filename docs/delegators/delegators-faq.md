<!--
order: 2
-->

# Delegator FAQ


## What is a delegators?

Token holders can get votes through the "delegation" command, and select the validators that they think are meaningful to the ecology. For these token holders, they are called the delegators.

Delegators play a critical role in the system, as they are responsible for choosing validators. Being a delegator is not a passive role: Delegators should actively monitor the actions of their validators and participate in governance. 

## Motive for voting

Voting is the primary way for token holders participate in the on-chain governance . Who will be the validators of the network. You want the validators to be very technically competent, because if something happens on the chain - something goes wrong, or a new feature appears, the validators will determine the direction of the network, and they need to be very professional and know what they are doing.

On the other hand is the community ecology aspect. validators need to work together. If there are any problems, then they need to be put in a room. They need to be able to work together to be good leaders in their communities and to spread the word and be highly transparent.

You will choose whether to become a delegator of okexchain. So please vote seriously, because the future of the chain depends on those votes and who will manage it.


## Choosing a validator

In order to choose their validators, delegators have access to a range of information directly in [OKLink](https://www.oklink.com/okexchain-test) or other OKExChain block explorers.

- **Validator's moniker**: Name of the validator candidate.
- **Validator's description**: Description provided by the validator operator.
- **Validator's website**: Link to the validator's website.

## Directives of delegators

Being a delegator is not a passive task. Here are the main directives of a delegator:

- **Perform careful due diligence on validators before delegating.** If a validator misbehaves, part of their total stake, which includes the stake of their delegators, can be slashed. Delegators should therefore carefully select validators they think will behave correctly.
- **Actively monitor their validator after having delegated.** Delegators should ensure that the validators they delegate to behave correctly, meaning that they have good uptime, do not double sign or get compromised, and participate in governance. They should also monitor the commission rate that is applied. If a delegator is not satisfied with its validator, they can unbond or switch to another validator (Note: Delegators do not have to wait for the unbonding period to switch validators. Rebonding takes effect immediately).
- **Participate in governance.** Delegators can and are expected to actively participate in governance. A delegator's voting power is proportional to the size of their bonded stake. If a delegator does not vote, they will inherit the vote of their validator(s). If they do vote, they override the vote of their validator(s). Delegators therefore act as a counterbalance to their validators.




