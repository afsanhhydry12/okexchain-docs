# DEX Traders Overview

## Introduction

In OKExChain-OKExDEX, users who want to exchange coins are called DEX Traders.

OKExChain-OKExDEX, as the first project of the OKExChain ecosystem, is a middleware that can freely issue DEX. With the design concept of "everyone can create DEX", it provides various basic functions needed to operate a DEX. 

Validators provide the machine configuration required by OKExChain, DEX Operators provide the tokenpair, and DEX Trader can realize the currency exchange function.


## Get market information

OKExChain-OKExDEX provides basic functions. Orders and matching occur on the chain, but these are not enough. Trading users need to use a more friendly UI page to help their trading decisions.About this part of the program construction is solved by other projects of OKExChain-OKExDEX ecology, we provide a [template](https://www.okex.com/dex-test) about data display.

But at the same time, OKExChain itself is an open source project, anyone can develop their own set of UI to help DEX Trade make better trading decisions.


## About Matching System

For a practical guide on matching engine , click [periodic auction](../concepts/periodic-auction.html).
