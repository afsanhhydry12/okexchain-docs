# DEX Operators Overview


## Introduction

The DEX operator is an entity that operates a decentralized exchange. His core assets are the digital asset trading pairs he operates, and his value is reflected through the operation of digital asset trading pairs. Generally speaking, the DEX operator only needs to provide  basic liquidity supply.

### Why do we need DEX operators?

Both centralized and decentralized exchanges aim to solve the exchange needs of digital assets.

From the development history of human beings, we can realize that at the same technical level, centralization is the most effective way to improve efficiency, that is, let professionals do professional things. This is why centralized exchanges that focus on efficiency develop more quickly than decentralized exchanges.

But not all things need to be solved in the most efficient way. After the centralized exchanges meet the needs that require efficiency in the exchange process, those neglected scenarios, such as scenario with security and fairness, also urgently need solving. In the blockchain world, this is reflected in the credit risk of fund entrustment.  The centralized exchanges and decentralized exchanges are not trying to replace each other but to supplement each other.

Furthermore, an exchange actually plays the role of ** building trust **  in the ecosystem, and so does a decentralized exchange. Thus, the operator of a decentralized exchange is the subject of this role. However, in the traditional concept, the subject of the role of  ** building trust **  is usually defined as the technology, that is, the public chain itself. But based on the market feedback for such projects in these years, we conclude that users do not necessarily agree to this kind of plan.

We think the reason is that there is a lack of operating entities. Just like the e-commerce model we are exposed to, Taobao is a platform, and it is Taobao sellers who really provide services to users.  As a platform, Taobao provides trust-building service for the sellers, but it does not mean that the merchants are not required to do any commercial operations. As for exchanges, the traditional decentralized exchange model puts too much emphasis on the platform and ignores the merchants, which are the main operating entities.

Therefore, the DEX operator of OKExChain is proposed to solve the problem of operating entities.

## Set Up a Website

Optionally, the DEX operator can choose to open a website of its own to enhance its credibility and facilitate other partners in the ecosystem to get more information about you, but this is not necessary.

For a practical guide on how to become a DEX operator, click [DEX Operators Guide (CLI)](../dex-operators/dex-operators-guide-cli.html).
